<?php
use Pages;
use Block;

class Section {
	protected $pdo;
	protected $page;
	protected $blocks;

    public function __construct($pdo, $data) {
		$this->pdo = $pdo;
        foreach($data as $key => $value) {
           $this->$key = $value;
        }
    }

    public function getPage() {
    	if (!$this->page) {
    		Pages::init($this->pdo);
    		$this->page = Pages::get($this->page_id);
    	}

    	return $this->page;
    }

    public function getBlocks() {
    	if (!$this->blocks) {
			$sql = "SELECT * FROM `block` WHERE section_id = ".$this->id." ORDER BY sort ASC";
			$sql = $this->pdo->prepare($sql);
			$sql->execute();
			$this->blocks = array();
			while($block = $sql->fetch()) {
				$this->blocks[] = new Block($this->pdo, $block);
			}
    	}

    	return $this->blocks;
    }

}
