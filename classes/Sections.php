<?php
use Section;

class Sections {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function get($id) {
		$sql = "SELECT * FROM  `section` WHERE id = ".self::$pdo->quote($id);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$sections = array();
		$section = $sql->fetch();
		if ($section) {
			$section = new Section(self::$pdo, $section);
		}

		return $section;
	}
}
