<?php
use Page;

class Pages {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function getAll() {
		$sql = "SELECT * FROM  `page` ORDER BY sort ASC ";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$pages = array();
		while($page = $sql->fetch()) {
			$pages[] = $page;
		}

		return $pages;
	}

	static function getBySlug($slug) {
		$sql = "SELECT * FROM  `page` WHERE slug = ".self::$pdo->quote($slug);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$pages = array();
		$page = $sql->fetch();
		if ($page) {
			$page = new Page(self::$pdo, $page);
		}

		return $page;
	}

	static function get($id) {
		$sql = "SELECT * FROM  `page` WHERE id = ".self::$pdo->quote($id);
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$pages = array();
		$page = $sql->fetch();
		if ($page) {
			$page = new Page(self::$pdo, $page);
		}

		return $page;
	}

	static function getFirst() {
		$sql = "SELECT * FROM  `page` ORDER BY sort ASC";
		$sql = self::$pdo->prepare($sql);
		$sql->execute();
		$pages = array();
		$page = $sql->fetch();
		if ($page) {
			$page = new Page(self::$pdo, $page);
		}

		return $page;
	}
}
