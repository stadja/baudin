<?php
require 'vendor/autoload.php';
include_once('.env');

use Passwords;
use Api;
use Pages;

session_start(); //by default requires session storage

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['db']['host']   = DB_HOST;
$config['db']['user']   = DB_USER;
$config['db']['pass']   = DB_PASS;
$config['db']['dbname'] = DB_DBNAME;

// Create app
$app = new \Slim\App(['settings' => $config]);

// Get container
$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

// Register provider
$container['flash'] = function () {
	return new \Slim\Flash\Messages();
};

// Register component on container
$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig('templates', [
		'cache' => 'cache',
		'auto_reload' => true,
		'debug' => true
	]);

	// Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

	return $view;
};

$app->get('/admin', function ($request, $response, $args) {
	Password::init($this->db);
	Pages::init($this->db);
	$slug = 'empty';
	$firstPage = Pages::getFirst();
	if ($firstPage) {
		$slug = $firstPage->slug;
	}
	if (Password::isLogged()) {
		return $response->withRedirect($this->router->pathFor('admin_page', ['page' => $slug]));
	} else {
		return $this->view->render($response, 'login.html.twig', [
			'messages' => $this->flash->getMessages()
		]);
	}
})->setName('admin');

$app->post('/admin', function ($request, $response, $args) {
	Password::init($this->db);
	$data = $request->getParsedBody();
	$isOk = true;

	if (!isset($data['password'])) {
		$isOk = false;
		$this->flash->addMessage('error', 'Aucun mot de passe n\'a été fourni');
	} elseif (!Password::test(filter_var($data['password'], FILTER_SANITIZE_STRING))) {
		$isOk = false;
		$this->flash->addMessage('error', 'Le mot de passe ne correspond pas');
	}

	if ($isOk) {
		$cookie = Password::setCookie();
	}

	return $response->withRedirect($this->router->pathFor('admin'));
})->setName('login');

$app->post('/admin/new_password', function ($request, $response, $args) {
	Password::init($this->db);
	$newPassword = Password::reinitLogin();
	if ($newPassword) {
		if(Password::sendPasswordEmail($newPassword)) {
			$this->flash->addMessage('info', 'Un nouveau mot de passe vous a été envoyé par email, vous le receverez d\ici peu de temps !');
		}
	}

	return $response->withRedirect($this->router->pathFor('admin'));
})->setName('forget_password');

$isLoggedMW = function ($request, $response, $next) {
	Password::init($this->db);
	if (Password::isLogged()) {
    	$response = $next($request, $response);
	} else {
    	$response = $response->withRedirect($this->router->pathFor('admin'));
	}

    return $response;
};

$app->get('/admin/pages/{page}', function ($request, $response, $args) {
	Pages::init($this->db);
	$page = Pages::getBySlug($args['page']);

	return $this->view->render($response, 'admin.page.html.twig', [
		'pages' => Pages::getAll(),
		'pageActive' => $page,
		'messages' => $this->flash->getMessages()
	]);
})
	->add($isLoggedMW)
	->setName('admin_page')
;

$app->get('/admin/sections/{section}', function ($request, $response, $args) {
	Pages::init($this->db);
	Sections::init($this->db);
	$section = Sections::get($args['section']);

	return $this->view->render($response, 'admin.section.html.twig', [
		'pages' => Pages::getAll(),
		'pageActive' => $section ? $section->getPage() : false,
		'sectionActive' => $section,
		'messages' => $this->flash->getMessages()
	]);
})
	->add($isLoggedMW)
	->setName('admin_section')
;

$app->post('/api/sort', function ($request, $response, $args) {
	Api::init($this->db);
	$parsedBody = $request->getParsedBody();
	$test = Api::sort($parsedBody['objectClass'], $parsedBody['orders']);
	return $response;
})
	->setName('api_sort')
;

$app->post('/api/post', function ($request, $response, $args) {
	Api::init($this->db);

	$parsedBody = $request->getParsedBody();
	$class = $parsedBody['objectClass'];
	unset($parsedBody['objectClass']);
	$id = $parsedBody['id'];
	unset($parsedBody['id']);
	$redirect = $parsedBody['redirect'];
	unset($parsedBody['redirect']);

	$action = '';
	if (isset($parsedBody['action'])) {
		$action = $parsedBody['action'];
		unset($parsedBody['action']);
	}

	if ($action == 'delete') {
		if (Api::delete($class, $id)) {
			$this->flash->addMessage('success', 'La suppression a été effectuée');
		} else {
			$this->flash->addMessage('error', 'Erreur lors de la suppression');
		}
	} elseif ($id == 'new') {
		if (Api::post($class, $parsedBody)) {
			$this->flash->addMessage('success', 'La création a été effectuée');
		} else {
			$this->flash->addMessage('error', 'Erreur lors de la création');
		}
	} else {
		if (Api::put($class, $id, $parsedBody)) {
			$this->flash->addMessage('success', 'La modification a été effectuée');
		} else {
			$this->flash->addMessage('error', 'Erreur lors de la modification');
		}
	}

	return $response->withRedirect($redirect);
})
	->setName('api_post')
;

$app->get('/', function ($request, $response, $args) {
	Pages::init($this->db);
	$firstPage = Pages::getFirst();
	return $this->view->render($response, 'index.html.twig', [
		'pages' => Pages::getAll(),
		'pageActive' => $firstPage,
		'messages' => $this->flash->getMessages()
	]);
})->setName('home');

$app->get('/sitemap.xml', function ($request, $response, $args) {
	Pages::init($this->db);

	$response = $response->withHeader('Content-type', 'applicationx/ml');
	return $this->view->render($response, 'sitemap.xml.twig', [
		'pages' => Pages::getAll(),
		'domain' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']
	]);
})
	->setName('sitemap')
;

$app->get('/{page}', function ($request, $response, $args) {
	Pages::init($this->db);
	$page = Pages::getBySlug($args['page']);

	return $this->view->render($response, 'index.html.twig', [
		'pages' => Pages::getAll(),
		'pageActive' => $page,
		'messages' => $this->flash->getMessages()
	]);
})
	->setName('page')
;

// Run app
$app->run();
